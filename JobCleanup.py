#!/usr/bin/python

"""
The lifetime of a task:

|- 24h -|                           # the job maximum allowed WallTime (JobUniverse == 5)
|----- 23d ----|                    # the maximum period of time that task/jobs resubmissions are allowed (JobUniverse == 7/5)
               |-- 7d --|           # the maximum time a job can stay in idle state (JobUniverse == 5)
|--------- 30d ---------|           # the actual job + task - lifetime (JobUniverse == 7)
                        |-- 10d --| # the period of time for which we set the classad "LeaveJobInQueue" to true for 
                                    # the task (JobUniverse == 7) - in that whay we prevent the condor daemon from deleting the task
|------------- 40d ---------------| # the extended lifetime of a dag(task) (JobUniverse == 7)

The script sequence:
1. Make a condor_q and put output in a list in the script
2. Find all the dags that are in the 10d 'LeaveJobInQueue' period (CRAB_TaskSubmitTime > 30days) - they should be in one of the following statuses: Hold(5)-H|Comleted(4)-C|Removed(3)-X
3. Start cleaning:
3.1. Clear the spool dir
3.2. Clear the webdir
4. Set the classad 'LeaveJobInQueue' to False - in that way the condor daemon will delete the job from the queue and do that with the Condorqedit:
4.1 Use the 'edit' method from the Schedd class

"""

import htcondor
import classad

import sys
import os
import json

from datetime import datetime 
from CrabDag import CrabDag
from shutil import rmtree
from JobCleanupAlarms import *

import pprint
import logging
import warnings
import pwd
import time

LOG_FILE = '/var/log/crab/JobCleanup.log'

TASK_LIFETIME = 38*24*60*60 #assuming 38 days just in case
TASK_EXTEND_LIFETIME = 8*24*60*60 #assuming 8 days after CRAB_TaskEndTime
HOSTNAME = os.uname()[1]
def getUserName():
        return pwd.getpwuid( os.getuid())[0]
USERNAME = getUserName()
DRYRUN = False


# todo - to move these as internal methods to the crabDag class 
def clearDag(crabDag):
    homeDir="/home/grid/%s/%s"%(crabDag.task["Owner"], crabDag.task["Crab_ReqName"])
    spoolDir=crabDag.task["Iwd"]
    if DRYRUN:
	crabDag.isCleared = True
	logging.info("-----------------------------------")
        logging.info("Task: %s %s is going to be cleared." % (crabDag.clusterId, crabDag.name))
	# logging.info("currentTaskLifeTime = %d" % crabDag.dagLifeTime)
	# logging.info("homeDir: %s" % homeDir)
	# logging.info("spoolDir: %s" % spoolDir)
        if os.path.exists(homeDir):
            logging.info("Deleting homeDir: %s" % homeDir)
        if os.path.exists(spoolDir):
	    logging.info("Deleting spoolDir: %s" % spoolDir)
	return
    else:
	crabDag.isCleared = True
        if os.path.exists(homeDir):
            logging.info("Deleting homeDir: %s" % homeDir)
	    try:
                rmtree(homeDir)
	    except:
		crabDag.isCleared = False
                deletionAlarm = DeletionAlarm(homeDir)
                warnings.warn(deletionAlarm, stacklevel=5)
        if os.path.exists(spoolDir):
	    logging.info("Deleting spoolDir: %s" % spoolDir)
	    try:
                rmtree(spoolDir)
	    except:
                crabDag.isCleared = False
                deletionAlarm = DeletionAlarm(spoolDir)
                warnings.warn(deletionAlarm, stacklevel=5)
		


def clearDagFromQueue(dagList, scheddObject):
    logging.info("List of ClusterIds to be cleared from the queue: %s" % dagList)
    #pprint.pprint(dagList)
    if DRYRUN:
	pass
	# scheddObject.edit(dagList, "LeaveJobInQueue_dryrun", 'False')
    else:
	scheddObject.edit(dagList, "LeaveJobInQueue", 'False')
        scheddObject.act(htcondor.JobAction.Remove, dagList) 


def prepareDagList(schedd):
    # 1. Make a condor_q
    scheddAlarm = None
    try:
        constraint = '( (JobUniverse =?= 7) )'
	projection =  ["ClusterId",
			"ProcId",
			"CRAB_UserHN",
			"CRAB_ReqName",
			"CRAB_TaskSubmitTime",
			"CRAB_TaskEndTime",
			"QDate",
			"DAGManJobId",
			"DAG_NodesQueued",
			"JobStatus",
			"JobUniverse",
			"Owner",
			"Iwd"]
        results = schedd.query(constraint, projection)
    except:
        scheddAlarm = ScheddAlarm("")
        warnings.warn(scheddAlarm, stacklevel=5)

    # fill everything in a list of CrabDag objects
    dags = []
    for dag in results:
        crabDag = None
        try:
            crabDag = CrabDag(dag)
	except KeyAlarm:
            continue
	else:
	    dags.append(crabDag)

    # logging.debug("List of dags: %s \n" % (pprint.pformat(dags)))
    # explicitly free the memory consumed from results and avoid later refferences
    del results

    # 2. Find all the dags ... - using Qdate here because some very old tasks do miss the classad CRAB_TaskSubmitTime
    # for dag in dags:
    #     dag.dagLifeTime = (dag.task["ServerTime"] - dag.task["Qdate"])
    #     logging.debug("currentTaskLifeTime = %d" % dag.dagLifeTime)
    #     if dag.dagLifeTime > TASK_LIFETIME:
    # 	    # done - to check if the dag status is in hold(5)||completed(4)||removed(3) - before take any action
    #         expectedDagStatus = [5, 4, 3]
    #         if dag.task["JobStatus"] in expectedDagStatus:
    #             dag.isReadyToClear = True
    #         else:
    #             staleTaskAlarm = StaleTaskAlarm(dag)
    #             warnings.warn(staleTaskAlarm, stacklevel=5)
    # return dags

    # 2. Find all the dags ... - using Qdate here because some very old tasks do miss the classad CRAB_TaskSubmitTime
    for dag in dags:
        if 'CRAB_TaskEndTime' in dag.task.keys():
            dag.dagExtendLifeTime = (dag.task["ServerTime"] - dag.task["CRAB_TaskEndTime"])

	else:
            dag.dagExtendLifeTime = (dag.task["ServerTime"] - (dag.task["Qdate"] + (TASK_LIFETIME - TASK_EXTEND_LIFETIME)))
	    logging.info("ClusterId: %s: Missing kew 'CRAB_TaskEndTime': Approximate 'currentTaskExtendLifeTime'; Crab_ReqName: %s; Qdate: %s." %  (dag.task["ClusterId"], dag.name, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(dag.task["Qdate"])) ))

	logging.debug("ClusterId: %s: currentTaskExtendLifeTime = %d days; Qdate: %s." % (dag.clusterId, (dag.dagExtendLifeTime//60//60//24), time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(dag.task["Qdate"]))))

        if dag.dagExtendLifeTime >= TASK_EXTEND_LIFETIME:
            # done - to check if the dag status is in hold(5)||completed(4)||removed(3) - before take any action
            expectedDagStatus = [5, 4, 3]
            if dag.task["JobStatus"] in expectedDagStatus:
                dag.isReadyToClear = True
		logging.debug("ClusterId: %s: Added for deletion." % dag.clusterId)
            else:
                staleTaskAlarm = StaleTaskAlarm(dag)
		logging.debug("ClusterId: %s: Not Added for deletion." % dag.clusterId)
                warnings.warn(staleTaskAlarm, stacklevel=5)
    return dags


    
def main():
	
    """
    The Job home & spool dir Cleanup script.
    """
    logging.info("-----------------------------------")
    logging.info("JoubCleaner.py has been started at: %s, by username: %s" % (str(datetime.now()),USERNAME))

    schedd = htcondor.Schedd()
    currDagList = prepareDagList(schedd)

    dagsToDelete = []

    # 3. Clear the dags spool&home dir
    for dag in currDagList:
        if dag.isReadyToClear:
            clearDag(dag)

    # 4. Delete the dags from the que
    for dag in currDagList:
        if dag.isCleared:
    	    dagObject = "%s.%s" % (dag.task["ClusterId"], dag.task["ProcId"])
    	    dagsToDelete.append(str(dagObject))
    clearDagFromQueue(dagsToDelete, schedd)

        
if __name__ == "__main__":
    warnings.resetwarnings()
    warnings.simplefilter("always", category=KeyAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=DeletionAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=StaleTaskAlarm, lineno=0, append=False)
    warnings.simplefilter("error", category=ScheddAlarm, lineno=0, append=False)
    
    logFormatter = logging.Formatter('%(asctime)s:%(levelname)s:%(module)s,%(lineno)d:%(message)s')
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    # Setting different loglevels for file logging and consoleoutput     
    fileHandler = logging.FileHandler(LOG_FILE)
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel(logging.INFO)
    rootLogger.addHandler(fileHandler)
    
    # Setting the output of the StreamHandler to stdout in order for the alarms to be catched from the cron.wrapper.sh
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    consoleHandler.setLevel(logging.WARNING)
    rootLogger.addHandler(consoleHandler)
    # Temporary redirect stderr 
    sys.stderr = open('/dev/null', 'w')
    
    logging.basicConfig(filename=LOG_FILE,level=logging.INFO,format='%(asctime)s:%(levelname)s:%(module)s,%(lineno)d:%(message)s')
    
    main()
