#!/usr/bin/python

import classad
import htcondor
import sys
import logging
import urllib

# Setting log file location and formatting details
LOG_FILE = '/var/log/crab/CMSLPCRoute.log'

# To fetch the list of LPC users from the LPC CE
def fetchLPCUsers(): 
    # Making sure CMSLPC_USER_URL is defined
    logging.debug("Checking for CMSLPC_USER_URL..")
    if 'CMSLPC_USER_URL' not in htcondor.param:
        logging.error("CMSLPC_USER_URL is undefined. No CMSLPC routes will be generated")
        sys.exit(0)

    # Fetching the list of users from the URL
    cmslpc_user_url = htcondor.param.get("CMSLPC_USER_URL")
    urlfd = urllib.urlopen(cmslpc_user_url)

    logging.debug("Fetching the DNs of LPC users")
    users = ""
    # Reading the fetched file line by line and
    # adding to a comma separated string 'users' 
    for line in urlfd.readlines():
        dn = line.strip()
        if not dn or dn.startswith("#"):
            continue
        users += str(dn)
        users += ","
    logging.debug("List of users fetched from LPC CE is as follows:\n%s" % users)
    return users

# To generate route for CMS LPC jobs
def generateLPCRoute(users):
    routerEntry = classad.ClassAd()
    routerEntry["Name"] = "CMSLPC Jobs"
    routerEntry["TargetUniverse"] = 9
    routerEntry["GridResource"] = "condor cmslpc-ce.fnal.gov cmslpc-ce.fnal.gov:9619"
    req_expr = '((TARGET.DESIRED_Sites=?="T3_US_FNALLPC") && (TARGET.TaskType=!="ROOT") && (TARGET.LPCRouted=!="true") && stringListMember(TARGET.x509userproxysubject, "%s", ","))' % users
    routerEntry["Requirements"] = classad.ExprTree(str(req_expr))
    routerEntry["set_LPCRouted"] = True
    routerEntry["set_MemoryUsage"] = 0
    routerEntry["MaxJobs"] = 100
    routerEntry["MaxIdleJobs"] = 100
    logging.info("Generated entry route was:\n%s" % routerEntry)
    print routerEntry

# Hold jobs for NON LPC users wanting to run at LPC
def holdNonLPCUsers(users):
    routerEntry = classad.ClassAd()
    routerEntry["Name"] = "Non CMSLPC User Jobs"
    routerEntry["TargetUniverse"] = 9
    routerEntry["GridResource"] = "invalid-host"
    req_expr = '((TARGET.DESIRED_Sites=?="T3_US_FNALLPC") && (TARGET.TaskType=!="ROOT") && (TARGET.LPCRouted=!="true") && !stringListMember(TARGET.x509userproxysubject, "%s", ","))' % users
    routerEntry["Requirements"] = classad.ExprTree(str(req_expr))
    routerEntry["set_LPCRouted"] = True
    routerEntry["set_MemoryUsage"] = 0
    routerEntry["set_PeriodicHold"] = True
    routerEntry["EditJobInPlace"] = True
    routerEntry["MaxJobs"] = 100
    routerEntry["MaxIdleJobs"] = 100
    routerEntry["set_PeriodicHoldReason"] = "User not part of the LPC user list. Job HELD"
    logging.info("Generated entry route was:\n%s" % routerEntry)
    print routerEntry

if __name__ == "__main__":
    logging.basicConfig(filename=LOG_FILE,level=logging.INFO,format='%(asctime)s: [%(levelname)s] %(message)s')
    logging.info("CMSLPCRoute invoked\nGenerating a new CMS LPC route")
    lpc_users = fetchLPCUsers()
    generateLPCRoute(lpc_users) 
    holdNonLPCUsers(lpc_users)
