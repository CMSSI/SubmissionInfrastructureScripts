#!/usr/bin/python

"""
The Overflow module for the JobAutoTuner.

"""

from __future__ import print_function, division

import htcondor
import classad

import re
import sys
import os
import pwd
import json, ast
import yaml
import httplib, urllib2
import pprint
import logging
import warnings
import JobAutoTunerAlarms

import numpy as np

os.sys.path.append('/data/srv/SubmissionInfrastructureScripts/')
os.sys.path.append('/data/srv/SubmissionInfrastructureScripts/WMCore/src/python/')

from JobAutoTunerAlarms import *
from WMCore.Services.SiteDB.SiteDB import SiteDBJSON

import pdb

from time import time
from time import sleep

from datetime import datetime

# from enum import Enum, unique
from pprint import pprint
from pprint import pformat

# from WMCore.Services.SiteDB.SiteDB import SiteDBJSON
from Cache import Cache
from Cache import CacheFlag

# Setting log file location and formatting details
LOG_FILE = '/var/log/crab/Overflow.log'

# @unique
# class OverflowLevel(Enum):
# class OverflowLevel(IntFlag)
class OverflowLevel(int):
    """
    A simple enum class that is used for marking the 'Level' at which the overflow is going to happen.
    The flags defined here are used for bitwise checks which 'Level' of overflow is enabled.
    This separation is actually telling us how do we set our granularity -
    How (at which level) do we split the batch of initial entities that we are
    going to work with. The 'Type' of overflow should come with a different class.
    These bit flags are strongly related with what we have to put in the constraint
    clause for the JobRouter ().
    """
    # todo - These 'Levels' should all be configurable through the Configurator

    PERTASK = 1
    PERJOB = 2
    PERBLOCK = 4
    PERFILE = 8
    PERDATASET = 16
    enableMask = 31
    disableMask = 0

class OverflowType(int):
    """
    A simple enum class that is used for marking the 'Type' of overflow that is going to be applied.
    The flags defined here are used for bitwise checks which 'Type' of overflow is enabled.
    This separation is actually telling us what kind of rules do we apply in order
    to decide 'if' and 'where' to send the jobs. These bit flags are strongly
    related with the Estimator and DestGenerator classes.
    """
    # todo - These 'Types' should all be configurable through the Configurator

    GEO = 1
    TIER1 = 2
    TIER2 = 4
    DATALOCATION = 8
    LOCALLOAD = 16
    SRCLOAD = 32
    DSTLOAD = 64
    enableMask = 127
    disableMask = 0

class InfoCollector:
    """
    The class that is intended to collect the information needed by the overflow (both static and dynamic).
    """

    def __init__(self, htCollector , config):
        self.initMessage = "=================== An InfoCollector object instance start! ==================="
        logging.debug(self.initMessage)

        self.config = config

        # note:
        # in order to use the WMCore interface to SideDB we should have
        # the package python-httplib2 installed
        # note:
        # should take into account that whenever we use WMCore to query
        # SiteDB it is maintaining its own cache and its timeouts should
        # be added to the ones from the local cache for that script.
        # note:
        # currently the cache file that WMCore is creating stays
        # in /tmp/.wmcore_cache_$(some_hashvalue) and is available as
        # long as the object instance from the class SiteDBJSON is alive
        # in our case as long as the infoCollector object is alive
        # todo - to make either this one or the local cache available
        #        through the consequent runs of the script otherwise
        #        we are going to hi steDB and cmsweb.cern.ch really hard
        # done - to install python-httplib2 in all the schedds with
        #        puppet or to configure WMCore to use pycurl instead
        # todo - what ever list is returned from sitedb it is going to be with
        #        unicode strings - needs to be converted in list of pure strings

        self.siteDBObj = SiteDBJSON(config={"key": self.config.serviceKey,
                                            "cert": self.config.serviceCert,
                                            "pycurl": True})

        self.htCollector = htCollector

        self.defSiteNames = ['T0_CH_CERN','T1_DE_KIT','T1_ES_PIC','T1_FR_CCIN2P3','T1_IT_CNAF','T1_RU_JINR','T1_RU_JINR_Disk','T1_UK_RAL','T1_UK_RAL_Disk','T1_US_FNAL','T1_US_FNAL_Disk','T2_AT_Vienna','T2_BE_IIHE','T2_BE_UCL','T2_BR_SPRACE','T2_BR_UERJ','T2_CH_CERN','T2_CH_CERN_AI','T2_CH_CERN_HLT','T2_CH_CERN_Wigner','T2_CH_CSCS','T2_CH_CSCS_HPC','T2_CN_Beijing','T2_DE_DESY','T2_DE_RWTH','T2_EE_Estonia','T2_ES_CIEMAT','T2_ES_IFCA','T2_FI_HIP','T2_FR_CCIN2P3','T2_FR_GRIF_IRFU','T2_FR_GRIF_LLR','T2_FR_IPHC','T2_GR_Ioannina','T2_HU_Budapest','T2_IN_TIFR','T2_IT_Bari','T2_IT_Legnaro','T2_IT_Pisa','T2_IT_Rome','T2_KR_KISTI','T2_KR_KNU','T2_MY_SIFIR','T2_MY_UPM_BIRUNI','T2_PK_NCP','T2_PL_Swierk','T2_PL_Warsaw','T2_PT_NCG_Lisbon','T2_RU_IHEP','T2_RU_INR','T2_RU_ITEP','T2_RU_JINR','T2_RU_PNPI','T2_RU_SINP','T2_TH_CUNSTD','T2_TR_METU','T2_TW_NCHC','T2_UA_KIPT','T2_UK_London_Brunel','T2_UK_London_IC','T2_UK_SGrid_Bristol','T2_UK_SGrid_RALPP','T2_US_Caltech','T2_US_Florida','T2_US_MIT','T2_US_Nebraska','T2_US_Purdue','T2_US_UCSD','T2_US_Vanderbilt','T2_US_Wisconsin','T3_BG_UNI_SOFIA','T3_BY_NCPHEP','T3_CH_CERN_CAF','T3_CH_CERN_HelixNebula','T3_CH_CERN_OpenData','T3_CH_PSI','T3_CH_Volunteer','T3_CN_PKU','T3_CO_Uniandes','T3_ES_Oviedo','T3_FR_IPNL','T3_GR_Demokritos','T3_GR_IASA','T3_HR_IRB','T3_HU_Debrecen','T3_IN_PUHEP','T3_IN_TIFRCloud','T3_IN_VBU','T3_IR_IPM','T3_IT_Bologna','T3_IT_Firenze','T3_IT_MIB','T3_IT_Opportunistic','T3_IT_Perugia','T3_IT_Trieste','T3_KR_KISTI','T3_KR_KNU','T3_KR_UOS','T3_MX_Cinvestav','T3_RU_FIAN','T3_RU_MEPhI','T3_RU_SINP','T3_TH_CHULA','T3_TW_NCU','T3_TW_NTU_HEP','T3_UK_GridPP_Cloud','T3_UK_London_QMUL','T3_UK_London_RHUL','T3_UK_SGrid_Oxford','T3_UK_ScotGrid_ECDF','T3_UK_ScotGrid_GLA','T3_US_ANL','T3_US_BU','T3_US_Baylor','T3_US_Brown','T3_US_Colorado','T3_US_Cornell','T3_US_FIT','T3_US_FIU','T3_US_FNALLPC','T3_US_FSU','T3_US_HEPCloud','T3_US_JHU','T3_US_Kansas','T3_US_MIT','T3_US_Minnesota','T3_US_NERSC','T3_US_NEU','T3_US_NU','T3_US_NotreDame','T3_US_OSG','T3_US_OSU','T3_US_Omaha','T3_US_Princeton_ARM','T3_US_Princeton_ICSE','T3_US_PuertoRico','T3_US_Rice','T3_US_Rutgers','T3_US_SDSC','T3_US_TACC','T3_US_TAMU','T3_US_TTU','T3_US_UB','T3_US_UCD','T3_US_UCR','T3_US_UCSB','T3_US_UIowa','T3_US_UMD','T3_US_UMiss','T3_US_UTENN','T3_US_UVA','T3_US_Vanderbilt_EC2','T3_US_Wisconsin']

        # infoDict is going to act as an internal cache for the class
        # todo - Here to make the queries for all the static information including the queries  to sitedb
        # todo - Here to read from external config or from condorConfig
        # todo - Here to make the query to the child collector and to create the
        #        list of all sites that have reached their limits as described
        #        by siteLimits. This set is going to be subtracted from the
        #        georeg subsets before generating the expanded sitelist

        self.__startTime = time()

        # the objects __dynamic/__staticCacheFlag now become global cache flags
        # the cache flags are going to be set per field from the __infoDict

        self.__dynamicCacheLifeTime = 10
        self.__dynamicCacheFlag = CacheFlag(False, self.__dynamicCacheLifeTime, 'global dynamicCacheFlag')

        self.__staticCacheLifeTime = 20
        self.__staticCacheFlag = CacheFlag(False, self.__staticCacheLifeTime, 'global staticCacheFlag')

        self.__cacheFlags = dict()
        self.__cacheFlags['static'] = dict()
        self.__cacheFlags['dynamic'] = dict()

        self.__infoDict = dict()
        self.__infoDict['static'] = dict()
        self.__infoDict['dynamic'] = dict()

        self.__cacheFlags['static']['scheddList'] = CacheFlag(False, self.__staticCacheLifeTime, 'scheddList')
        self.__infoDict['static']['scheddList'] = self.getScheddList()

        self.__cacheFlags['static']['siteNames'] = CacheFlag(False, self.__staticCacheLifeTime, 'siteNames')
        self.__infoDict['static']['siteNames'] = self.readSiteDB('site_names')

        self.__cacheFlags['static']['siteResources'] = CacheFlag(False, self.__staticCacheLifeTime, 'siteResources')
        self.__infoDict['static']['siteResources'] = self.readSiteDB('siteResources')

        self.__cacheFlags['static']['siteAssociations'] = CacheFlag(False, self.__staticCacheLifeTime, 'siteAssociations')
        self.__infoDict['static']['siteAssociations'] = self.readSiteDB('siteAssociations')

        self.__cacheFlags['static']['sites'] = CacheFlag(False, self.__staticCacheLifeTime, 'sites')
        self.__infoDict['static']['sites'] = self.readSiteDB('sites')

        self.__cacheFlags['dynamic']['siteLimits'] = CacheFlag(False, self.__dynamicCacheLifeTime, 'siteLimits')
        self.__infoDict['dynamic']['siteLimits'] = self.checkSiteLimits()

        # after initialising all the static info cache values set the cacheLastUpdate time of the global flag:
        self.__staticCacheFlag.checkExpiredAndSet()

        logging.debug("self.__infoDict: \n%s" % pformat(self.__infoDict))
        logging.debug("self.__cacheFlags: \n%s" % pformat(self.__cacheFlags))

        self.initMessage = "=================== An InfoCollector object instance end! ==================="
        logging.debug(self.initMessage)

    def __call__(self):
        logging.debug("Default __call__ method for InfoCollector is doing nothing.")

    def __externalSiteDBApi__(self, api):
        logging.debug("__externalSiteDBApi__ called for api: %s" % api)

        # This function may be called from the Configurator Class befor we have a
        # pure object instance so we have to hardcode at least one external
        # interface to call in such a case. If this one fails only then we return
        # the list with defSiteNames, which should be updated from times to times.
        # done - to convert the list of sites returned from SiteDB from unicode
        #        to str (htcondor is not accepting unicode lists)

        if api == "site_names":
            siteNames = []
            # # This part is working - temporary disabled in order not to hit CMSWeb
            # try:
            #     siteNames = self.siteDBObj.getAllCMSNames()
            #     siteNames = [str(x) for x in siteNames]
            #     # logging.debug("SiteNames retuned by  __externalSitedbApi__: \n%s" % pformat(siteNames))
            #     # logging.debug("dirs in: %s/:\n%s" % ('/tmp', pformat(os.listdir('/tmp'))))

            # except Exception as e:
            #     siteNames = []
            #     logging.debug("Could not query __externalSitedbApi__. Exception: %s" % e)

            if not bool(siteNames):
                logging.debug("Could not fetch %s from __externalSiteDBApi__. Returning Default value!" % api)
                siteNames = self.defSiteNames

            return siteNames

    def readSiteDB(self, api):
        if api == "site_names":
            # first to consult back the cache if the data exists and or if it is stale (check for cacheExpired)
            # the following line is wrong - because always calls the external function to set the default value,
            # but we are trying to avoid calling the external api as much as possible:
            # self.__infoDict['static'].setdefault('siteNames', self.__externalSiteDBApi__(api))
            # todo - here should make an explicit check if the value has been set before updating the cahceFlag
            if 'siteNames' not in self.__infoDict['static']:
                self.__infoDict['static']['siteNames'] = self.__externalSiteDBApi__(api)
                self.__cacheFlags['static']['siteNames'].checkExpiredAndSet()

            # done - Once the CacheLifeTime expires then it is not renewed
            #        We do not reset  the timer for that when we update the information in the cache
            if self.__cacheFlags['static']['siteNames']():
                self.__infoDict['static']['siteNames'] = self.__externalSiteDBApi__(api)
                self.__cacheFlags['static']['siteNames'].checkExpiredAndSet()

            siteNames = self.__infoDict['static']['siteNames']
            return siteNames

        if api == "site_resources":
            if 'siteResources' not in self.__infoDict['static']:
                self.__infoDict['static']['siteResources'] = self.__externalSiteDBApi__(api)
                self.__cacheFlags['static']['siteResources'].checkExpiredAndSet()

            if self.__cacheFlags['static']['siteResources']():
                self.__infoDict['static']['siteResources'] = self.__externalSiteDBApi__(api)
                self.__cacheFlags['static']['siteResources'].checkExpiredAndSet()

            siteResources = self.__infoDict['static']['siteResources']
            return siteResources

        if api == "site_associations":
            if 'siteAssociations' not in self.__infoDict['static']:
                self.__infoDict['static']['siteAssociations'] = self.__externalSiteDBApi__(api)
                self.__cacheFlags['static']['siteAssociations'].checkExpiredAndSet()

            if self.__cacheFlags['static']['siteAssociations']():
                self.__infoDict['static']['siteAssociations'] = self.__externalSiteDBApi__(api)
                self.__cacheFlags['static']['siteAssociations'].checkExpiredAndSet()

            siteAssociations = self.__infoDict['static']['siteAssociations']
            return siteAssociations

        if api == "sites":
            if 'sites' not in self.__infoDict['static']:
                self.__infoDict['static']['sites'] = self.__externalSiteDBApi__(api)
                self.__cacheFlags['static']['sites'].checkExpiredAndSet()

            if self.__cacheFlags['static']['sites']():
                self.__infoDict['static']['sites'] = self.__externalSiteDBApi__(api)
                self.__cacheFlags['static']['sites'].checkExpiredAndSet()

            sites = self.__infoDict['static']['sites']
            return sites

    def parseRegNames(self, regType, regList, initialSet):
        """
        A function used to parse a Set of elements (site names) and produce
        a subset according to the type and the list of regions passed to it.
        Supported region types are 'tier' && 'geo'.
        Note: Here the abbreviation 'regSuffix' comes from region.
        """
        subSet = set()
        for region in regList:
            if regType == 'tier':
                restring = "^%s_.+_.+$" % region
            if regType == 'geo':
                restring = "^T[0-9]_%s_.+$" % region
            # try: ...except Exception as e: warnings.warn()...
            regexp = re.compile(restring)
            for site in initialSet:
                if regexp.match(site):
                    subSet.add(site)
        subSet=frozenset(subSet)
        logging.debug("subSet generated with %s: %s\n" % (regList ,subSet))
        return subSet

    def __externalScheddList__(self):
        scheddAdds=[]
        scheddList={}
        regexp=re.compile('^crab3.*@vocms[0-9]+\.cern.ch$')

        for scheddAdd in self.htCollector.locateAll(htcondor.DaemonTypes.Schedd):
            scheddAdds.append(scheddAdd)
            scheddName = scheddAdd.get('Name')
            if regexp.match(scheddName):
                scheddList[scheddName]=htcondor.Schedd(scheddAdd)
        return scheddList

    def getScheddList(self):
        """
        A function used to create a list of schedd objects known to the collector
        and maintain it fresh. This information is considered stattic.
        """
        # todo - to check if it is worth keeping the list as a dict with a
        #        key value pair like {scheddName: scheddObject}
        if 'scheddList' not in self.__infoDict['static']:
            self.__infoDict['static']['scheddList'] = self.__externalScheddList__()
            self.__cacheFlags['static']['scheddList'].checkExpiredAndSet()

        if self.__cacheFlags['static']['scheddList']():
            self.__infoDict['static']['scheddList'] = self.__externalScheddList__()
            self.__cacheFlags['static']['scheddList'].checkExpiredAndSet()

        scheddList = self.__infoDict['static']['scheddList']
        return scheddList

    def __externalSiteLimits__(self):
        """
        A function used to query the external resources for the currently
        reached SilteLimtis.
        """
        # logging.debug("HTCondor collector address: %s" % self.config.htCollector)
        # logging.debug("HTCondor collector instance: %s" % self.htCollector)

        constraint="""
        AcctGroup =?= "analysis" &&
        JobUniverse == 5 &&
        HasBeenOverflowRouted =?= True
        """
        projection=['DESIRED_Sites', 'DESIRED_Sites_Orig', 'DESIRED_Sites_Diff', 'JobStatus', 'MATCH_GLIDEIN_CMSSite']

        siteLimits = {}
        queryResults = {}

        # # Temporarily disabling the Schedd queries
        # todo - to add a parameter to the function like:
        #        __externalSiteLimits__(self, infoProvider) or
        #        __externalSiteLimits__(self, extResource)
        #        in order to preserve the schedd queries as an option and to add
        #        more methods (more extResouces) for fetching the information
        #        like ES for example or the global collector
        #
        # scheddList = self.getScheddList()
        # # logging.debug("scheddList from  __externalSiteLimits__: \n%s" % pformat(scheddList))

        # # # Sequential method for querying all the analysis schedds:
        # # for scheddName, scheddObj in scheddList.iteritems():
        # #     qIterator = scheddObj.xquery(constraint, projection)
        # #     # scheddName = qIterator.tag()
        # #     queryResults.setdefault(scheddName, [])
        # #     for classAdd in qIterator:
        # #         queryResults[scheddName].append(classAdd)

        # # Parallel method for querying all the analysis schedds:
        # queries=[]
        # for scheddName, scheddObj in scheddList.iteritems(): 
        #     queries.append(scheddObj.xquery(constraint, projection))

        # for query in htcondor.poll(queries):
        #     scheddName = query.tag()
        #     queryResults.setdefault(scheddName, []) 
        #     queryResults[scheddName] += query.nextAdsNonBlocking()

        logging.debug("Query Results from  __externalSiteLimits__: \n%s " % pformat(queryResults))

        for siteName in self.readSiteDB('site_names'):
            siteLimits.setdefault(siteName, {})
            currOvJobsFromCounter = 0
            currOvJobsToCounter = 0
            for scheddName, addList in queryResults.iteritems():
                # Here we count the number of occurrences of a given site in the
                # classads returned from the previous query.
                # done - to change the `if` conditions here with the relevant
                #        strlist parsing lambdas which should return True if
                #        siteName `exists in` add['DESIRED_Sites_Orig/Diff']
                #        an example with a list of integers follows:
                #        outx = 2
                #        currOvJobsFromCounter += reduce((lambda inx, y: inx + 1 if y == outx else inx ), [2,2,3,5,1,4], 0)
                #        currOvJobsToCounter   += reduce((lambda inx, y: inx + 1 if y == outx else inx ), [2,2,3,5,1,4], 0)
                #
                # done - to add the clasAdd attribute 'MATCH_GLIDEIN_CMSSite'
                #        for the currently running site in order to do a real
                #        counting in currOvJobsToCounter
                # note - Here the lambdas should be kept as a single line
                try:
                    currOvJobsFromCounter += reduce((lambda counter, add:
                                                     counter + 1
                                                     if siteName in add['DESIRED_Sites_Orig'].split(',') and
                                                     add['JobStatus'] == 2 and
                                                     siteName != add['MATCH_GLIDEIN_CMSSite']
                                                     else
                                                     counter),
                                                    addList,
                                                    0)
                    currOvJobsToCounter   += reduce((lambda counter, add:
                                                     counter + 1
                                                     if siteName in add['DESIRED_Sites_Diff'].split(',') and
                                                     add['JobStatus'] == 2 and
                                                     siteName == add['MATCH_GLIDEIN_CMSSite']
                                                     else
                                                     counter),
                                                    addList,
                                                    0)
                except Exception as e:
                    logging.exception("Exception while setting __externalSiteLimits__: \n%s " % e)
                    # warnings.warn("WARNING: !!!An operator action Required Here!!!", category=SiteLimitsAlarm)
                    # if we have got to this exception we definitely do NOT count
                    # the site limits ergo can easily start flooding them
                    # that's why besides the logging message we call the warning
                    # alarm in order to call the Operator through it.
                    # todo - to finish all the alarm subclasses and reorganize
                    #        them in a proper way that the operator can be called
                    #        from there (by mail or any other method)
            siteLimits[siteName]['currOvJobsFrom'] = currOvJobsFromCounter
            siteLimits[siteName]['currOvJobsTo']   = currOvJobsToCounter

        return siteLimits

    def checkSiteLimits(self, siteName='all'):
        """
        A function used to check (using the current script's cache system) the
        various limits reached per Site. These can be compared lately with the
        limits set from the Configurator Class. This information is considered
        dynamic.
        """

        if 'siteLimits' not in self.__infoDict['dynamic']:
            self.__infoDict['dynamic']['siteLimits'] = self.__externalSiteLimits__()
            self.__cacheFlags['dynamic']['siteLimits'].checkExpiredAndSet()

        if self.__cacheFlags['dynamic']['siteLimits']():
            self.__infoDict['dynamic']['siteLimits'] = self.__externalSiteLimits__()
            self.__cacheFlags['dynamic']['siteLimits'].checkExpiredAndSet()

        if siteName == 'all':
            siteLimits = self.__infoDict['dynamic']['siteLimits']
        else:
            siteLimits = self.__infoDict['dynamic']['siteLimits'][siteName]

        # logging.debug("siteLimits from checkSiteLimits(): %s" % siteLimits)
        return siteLimits


class Estimator:
    """
    The Class that is used to estimate the need for overflow according to external criteria
    and the current situation given the information coming from the InfoCollector and
    the specific object that is querying for the Estimator.
    The load estimation can be done by two different approaches:
    1. Indirectly by using all the information about the current status of the jobs(idle time etc...).
    2. Directly by observing the current status of the resources.
    """
    # note - We are about to apply a limitation related to a previously set
    #        silteLimits for the max number of overflow jobs from/to a site
    #        The 'From' limitation need to be but here
    #        The 'To' limitation need to be put in the DesGenerator class

    def __init__(self, infoCollector, config, ovType):
        self.initMessage = "=================== An Estimator object instance start! ==================="
        logging.debug(self.initMessage)

        self.infoCollector = infoCollector
        self.config = config
        self.ovType = ovType
        logging.debug("config.maxIdleTime = %s" % self.config.maxIdleTime)

        self.initMessage = "=================== An Estimator object instance end! ==================="
        logging.debug(self.initMessage)


    def needOverflow(self, jobObject):
        """
        A function used to estimate if the application of the overflow mechanism
        is needed on the current jobObject. Every sub component of that function
        (related to all the enabled overflow mechanisms) is supposed to take its
        decision independently and put it in a weighted list of decision values.
        The weights themselves must be kept in a different list in a manner that
        will allow functions like numpy.random.choice && numpy.random.multinomial
        to be applied later. The list of weights must be normed to 1 and can be
        statically defined or we can make it dynamic in the future and apply more
        elaborate mechanisms for estimating the optimal weights according to the
        prompt feedback about the reaction of the system. The final result of the
        function can be constructed in two different ways:
        1. We can either return the decision from the highest weight - in which
           case we will have strictly defined discrete criteria.
        2. We can return the weighted Sum of all the fields from the decisionList
           In which case we will have more complex and 'multi-layered' criteria.
        """

        # todo - The same mechanism but at the level of subsets and finding their
        #        correct intersections must happen in the DestGenerator.

        # maxIdleTime must be converted from (min.) to (sec.) or
        # currIdleTime from (sec.) to (min.)

        returnVal = False
        currIdleTime = False
        currIdleTime = jobObject["ServerTime"] - jobObject["QDate"]
        currIdleTime = int(currIdleTime/60)

        decisionVal = False
        decisionList = []
        decisionWeights = []

        # This is the list of common Decisions which reflects the mandatory list
        # of common criteria that should be fulfilled, like idle time etc.
        commonDecisionList = []
        commonDecisionVal = False

        if currIdleTime and currIdleTime > 0 and \
           jobObject["JobStatus"] == 1 and \
           (currIdleTime - self.config.maxIdleTime) > 0:
            decisionVal = True
            commonDecisionList.append(decisionVal)
            logging.debug("config.maxIdleTime : %s" % self.config.maxIdleTime)
            logging.debug("currentIdleTime: %s for: %s" % (currIdleTime, jobObject["CRAB_ReqName"]))
        # elif bool("Here to add more common checks"):
        #     pass
        else:
            # In order to make sure that the commonDecisionList is not empty we
            # a False element inside it because if we have reached that point it
            # is obvious that none of the mandatory conditions has been fulfilled
            commonDecisionList.append(False)

        # Applying an un-weighted '&&' operation on all the elements of commonDecisionList
        commonDecisionVal = reduce((lambda x, y: x and y ), commonDecisionList)
        logging.debug("commonDecisionVal: %s" % commonDecisionVal)

        # exit here if the common criteria has not been fulfilled for that jobObj
        if not commonDecisionVal:
            return commonDecisionVal

        decisionVal = False
        if self.ovType & OverflowType.GEO:
            if commonDecisionVal:
                decisionVal = True
            decisionList.append(decisionVal)
            decisionWeights.append(0.4)
            logging.debug("OverflowType.GEO: decisionVal: %s" % decisionVal)

        decisionVal = False
        if self.ovType & OverflowType.TIER1:
            if commonDecisionVal and \
               jobObject["DESIRED_Sites"].split(',').__len__() == 1 and\
               jobObject["DESIRED_Sites"].split('_')[0] == 'T1':
                siteName = jobObject["DESIRED_Sites"]
                logging.debug("self.infoCollector.checkSiteLimits(%s)['currOvJobsFrom']: %s" % (siteName, self.infoCollector.checkSiteLimits(siteName)['currOvJobsFrom']))
                # Including only the sites that have not reached their limits:
                if siteName in self.config.siteLimits and self.config.siteLimits[siteName]['maxOvJobsFrom'] > self.infoCollector.checkSiteLimits(siteName)['currOvJobsFrom']:
                    decisionVal = True
            decisionList.append(decisionVal)
            decisionWeights.append(0.6)
            logging.debug("OverflowType.TIER1: decisionVal: %s" % decisionVal)

        # We will go for the easier choice initially: the weighted sum
        # For that purpose first we translate the decisionList as:
        # False = -1
        # True = 1
        # After that we create the weighted Sum
        # logging.debug("decisionList: %s" % decisionList)

        decisionList = map((lambda x: 1 if x else -1), decisionList)
        returnVal = reduce((lambda x, y: x+y), np.multiply(decisionList, decisionWeights))

        # logging.debug("decisionList: %s" % decisionList)
        # logging.debug("decisionWeights: %s" % decisionWeights)
        logging.debug("Final needOverflow decision: %s" % returnVal)

        return bool(returnVal > 0)


class DestGenerator:
    """
    The Class that is used to generate the destinations to overflow to.
    It is going to use the InfoCollector in order to fetch the dynamic situation of the load.
    The load estimation can be done by two different approaches:
    1. Indirectly by using all the information about the current jobs status (idle time etc...).
    2. Directly by observing the current status of the resources.
    """

    def __init__(self, infoCollector, config, ovType):
        self.initMessage = "=================== A DestGenerator object instance start! ==================="
        logging.debug(self.initMessage)

        self.infoCollector = infoCollector
        self.config = config
        self.ovType = ovType

        self.initMessage = "=================== A DestGenerator object instance end! ==================="
        logging.debug(self.initMessage)

    def __call__(self):
        logging.debug("Default __call__ method for DestGenerator.")

    def siteListDiff(self, siteList1, siteList2):
        diffSet = set()
        siteList1 = siteList1.split(',')
        siteList2 = siteList2.split(',')
        for site in siteList1:
            if site not in siteList2:
                diffSet.add(site)
        for site in siteList2:
            if site not in siteList1:
                diffSet.add(site)
        if bool(diffSet):
            logging.debug("siteListDiff:\n %s\n =!= to:\n %s" % (pformat(siteList1), pformat(siteList2)))
            logging.debug("siteListDiff: difference:\n %s" % pformat(diffSet))
            return self.setToStrList(diffSet)
        else:
            logging.debug("siteListDiff:\n %s\n == to:\n %s" % (pformat(siteList1), pformat(siteList2)))
            return False

    def destSiteList(self, siteList):
        """
        This function must generate the new siteList based on the current DESIRED_SITES list passed as an argument
        to the function and according to that it should find the optionally expandable geographical regions then re generate
        the new (expanded) DESIRED_SITES list including all the additional sites from the regions in it
        (we are starting with a narrow set of sites and going to a wider one.)
        """
        ################################################################
        # testing the cacheLifeTime expiration:
        # logging.debug("-------- 1. calling infoCollector.readSitedb --------")
        # self.infoCollector.readSiteDB("site_names")
        # sleep(3)
        # logging.debug("-------- 2. calling infoCollector.readSitedb --------")
        # self.infoCollector.readSiteDB("site_names")
        ################################################################

        # The siteList has the so called type 'string list' - string representing a comma separated list of sitenames (without intervals)
        if isinstance(siteList, str):
            siteList = siteList.split(',')

        logging.debug("=================== A DestGenerator.destSiteList call! ===================" )
        logging.debug("Initial siteList: \n%s\n" % pformat(siteList))

        if not siteList:
            logger.warning("WARNING: Could not convert siteList from 'str' to 'list'")
            return siteList
            # todo - to create a type for common check alarms
            #        warnings.warn("WARNING: Could not convert siteList from str to list", category=CommonCheckAlarm)

        # done - to make the desired sites list a set then subtract the region names from this set,
        #        create a list of unique region names and then for any item from this list to call the reg generator
        #        for widening the subset and add it to the major set of desired sites

        siteListSet = set(siteList)

        geoRegSubsets = dict()
        tierRegSubsets = dict()

        # finding the existing tier and geo regions in the current siteList
        tierRegs = set()
        geoRegs = set()

        # for site in siteListSet:
        #     tierRegs.add(site.split('_')[0])
        # tiersRegs = frozenset(tierRegs)
        # recreating the tier subsets per geo region
        # tierSubsets = dict()
        # create the tier and geo subsets that exist in the current siteList
        # for tierReg in tierRegs:
        #     tierSubSet = self.infoCollector.parseRegNames(
        #         regType='tier',
        #         regList=[tierReg],
        #         initialSet=siteListSet)
        #     tierSubsets[tierReg] = tierSubSet
        # logging.debug("tierSubsets: \n%s\n" % pformat(tierSubsets))

        for site in siteListSet:
            geoRegs.add(site.split('_')[1])
        geoRegs = frozenset(geoRegs)

        for geoReg in geoRegs:
            if geoReg in self.config.enGeoRegs or self.config.enGeoRegs == ['ALL']:
                geoRegSubSet = self.infoCollector.parseRegNames(
                    regType='geo',
                    regList=[geoReg],
                    initialSet=siteListSet)
                geoRegSubsets[geoReg] = geoRegSubSet

        logging.debug("geoRegSubsets: \n%s\n" % pformat(geoRegSubsets))

        # Creating the set of tiers that exist in the siteList per geoReg
        # 1. find the tier regions ...(look bellow)
        for geoReg, geoRegSubset in geoRegSubsets.iteritems():
            tierRegs = set()
            for site in geoRegSubset:
                tierRegs.add(site.split('_')[0])
            # Allow overflow from T1 -> T2 && leave T3 untouched
            if 'T1' in tierRegs:
                tierRegs.add('T2')
            if 'T3' in tierRegs:
                tierRegs.remove('T3')
            tiersRegs = frozenset(tierRegs)
            tierRegSubsets[geoReg] = tiersRegs

        logging.debug("tierRegSubsets: \n%s\n" % pformat(tierRegSubsets))

        # here happens the geographical expansion of the siteList
        # for all the given geo regions (that exist in the current siteList)
        # create a subset of all available sites returned by the InfoCollector
        # and in the current siteList substitute the subset related to that geo region
        # with the newly generated (expanded) subset
        # this will alternatively include sites from tier levels(regions) that did not
        # exist in the initial siteList that why in a later step we need to find the
        # intersection of the siteList with the subset of all available sites returned by the InfoCollector
        # related  to the tier regions that existed in the initial siteList
        newGeoRegSubset = set()
        newTierRegSubset = set()
        for geoReg, geoRegSubset in geoRegSubsets.iteritems():
            newGeoRegSubset = self.infoCollector.parseRegNames(
                regType = 'geo',
                regList = [geoReg],
                initialSet = self.infoCollector.readSiteDB(api = 'site_names'))
            logging.debug("newGeoRegSubset before limits: %s"  % newGeoRegSubset)
            # Excluding the sites that have reached their limits:
            limGeoRegSubset = set()
            for siteName in newGeoRegSubset:
                if siteName in self.config.siteLimits and self.config.siteLimits[siteName]['maxOvJobsTo'] <= self.infoCollector.checkSiteLimits(siteName)['currOvJobsTo']:
                    limGeoRegSubset.add(siteName)
            newGeoRegSubset -= limGeoRegSubset
            logging.debug("newGeoRegSubset after limits: %s"  % newGeoRegSubset)

            # before substituting the geographically expanded subset:
            # 1. find the tier regions that have existed in the initial siteList - done in the previous step

            # 2. expand them with InfoCollector
            newTierRegSubset = self.infoCollector.parseRegNames(
                regType = 'tier',
                regList = tierRegSubsets[geoReg],
                initialSet = self.infoCollector.readSiteDB(api = 'site_names'))

            # 3. find the intersection between these two subsets and asign it to the newGeoRegSubset
            newGeoRegSubset &= newTierRegSubset

            if newGeoRegSubset > geoRegSubset:
                logging.debug("Expanding geoReg: \n%s\n" % pformat(geoReg))
                siteListSet -= geoRegSubset
                siteListSet |= newGeoRegSubset

        # logging.debug("Intermediate siteListSet: \n%s\n" % pformat(siteListSet))
        # generate subset of all available sites returned by the InfoCollector
        # related to the tier regions that existed in the initial siteList
        # and find the intersection between the set of the geographically expanded siteListSet and the newTierRegSubset
        # return the intersection of these both
        #
        # done - to create the intersections with the tier subsets per geo region
        #        (not create the intersection between the final siteList and the Tier subset
        #        but the intersection between the geo subset with the tier subset izisting within this subset)
        # newTierRegSubset = self.infoCollector.parseRegNames(
        #     regType='tier',
        #     regList=tierRegs,
        #     initialSet = self.infoCollector.readSiteDB(api = 'site_names'))
        # siteListSet &= newTierRegSubset

        logging.debug("Final siteListSet: \n%s\n" % pformat(siteListSet))

        # we must return the newSiteList in the same format as it was passed to the function
        # convert the siteListSet from a set type to the so called 'string list'
        newSiteList = self.setToStrList(siteListSet)
        return newSiteList

    def setToStrList(self, initSet):
        finList = ""
        for elem in initSet:
            if finList == "":
                finList = elem
            else:
                finList = "%s,%s" % (finList,elem)
        return finList

class Configurator:
    """
    The Configuration Class used to read configuration parameters from the
    htcondor module or an external .json file and convert them into the proper
    type as which they are about to be used later on.
    Regardless of the method used to set a cofiguragion parametr (either through
    condor_config or through configFile) all of them need to be properly
    prefixed. In our case with JAT_OVERFLOW_<ParameterName>.
    """

    # done - to move from a .json to .yaml config. The later is a superset of
    #        the of the former and provides more flexability

    # todo - not to be repeated from JobAutoTuner but to make it a common function
    # todo - to find a way to read values from crabConfig and obey the flag
    #        allowOverflow set by the users

    def __init__(self):
        self.initMessage = "=================== An Overflow Configurator instance start! ==================="
        logging.debug(self.initMessage)

        # This is the only parameter that is availiable only from condor_config
        self.configFileName = self.__readCondorConfig__("JAT_OVERFLOW_configFile", 'str')

        self.serviceKey = self.__readCondorConfig__("JAT_OVERFLOW_serviceKey", 'int')
        if not self.serviceKey:
            self.serviceKey = '/data/certs/servicekey.pem'
        logging.debug("serviceKey = %s" % self.serviceKey)

        self.serviceCert = self.__readCondorConfig__("JAT_OVERFLOW_serviceCert", 'int')
        if not self.serviceCert:
            self.serviceCert = '/data/certs/servicecert.pem'
        logging.debug("serviceCert = %s" % self.serviceCert)

        # The Overflow Level
        self.ovLevel = self.__readCondorConfig__("JAT_OVERFLOW_level", 'OverflowLevel')
        if not self.ovLevel:
            self.ovLevel = OverflowLevel.PERTASK | OverflowLevel.PERTASK
        logging.debug("overflowLevel = %s" % self.ovLevel)

        # The Overflow Type
        self.ovType = self.__readCondorConfig__("JAT_OVERFLOW_type", 'OverflowType')
        if not self.ovType:
            self.ovType = OverflowType.GEO
        logging.debug("overflowType = %s" % self.ovType)

        # The maximum Job Idle Time allowed beofeore an Overflow route is generated (min.)
        self.maxIdleTime = self.__readCondorConfig__("JAT_OVERFLOW_maxIdleTime", 'int')
        if not self.maxIdleTime:
            self.maxIdleTime = int(0.1*60)
        logging.debug("maxIdleTime = %s" % self.maxIdleTime)

        # The list of enabled geographical regions. Set to ['ALL'] if all regions are about to be enabled
        self.enGeoRegs = self.__readCondorConfig__("JAT_OVERFLOW_enGeoRegs", 'list')
        if not self.enGeoRegs:
            # self.enGeoRegs = ['IT', 'ES', 'DE']
            # self.enGeoRegs = ['ALL']
            self.enGeoRegs = []
        logging.debug("enGeoRegs = %s" % self.enGeoRegs)

        # The dict with the siteLimits. As an example:
        # {'T2_IT_Bari': {'maxOvJobsFrom': 3000}, 'T2_IT_Pisa': {'maxOvJobsTo': 1000}}
        self.siteLimits = False

        # First try - setup the siteLimits from file
        self.siteLimits = self.__readFileConfig__(self.configFileName, "JAT_OVERFLOW_siteLimits", "dict")

        # self.siteLimitsFileName = self.__readCondorConfig__("JAT_OVERFLOW_siteLimitsFile", 'str')
        # if self.siteLimitsFileName and os.path.isfile(self.siteLimitsFileName):
        #     with open(self.siteLimitsFileName) as self.siteLimitsFile:
        #         self.siteLimits = json.load(self.siteLimitsFile)
        #         self.siteLimits = ast.literal_eval(json.dumps(self.siteLimits)) # clean unicode chars from dict

        # Second try - setup siteLiimits from condor_config
        if not self.siteLimits:
            self.siteLimits = self.__readCondorConfig__("JAT_OVERFLOW_siteLimits", 'dict')

        # Third try - fallback to the minimal default
        if not self.siteLimits:
            # todo - the following dict to be created for all the sites visible
            #        from siteDB at the moment of the script execution - to use the InfoCollector
            #        The problem comes from the fact that at the moment when
            #        the Configurator is instantiated the Infocollector is still
            #        not having an instanced object (it needs the config object in its constructor)
            #
            # todo - to add few more siteLimits like maxOvJobsReadFrom once
            #        I can identify the source providing the data for the running jobs
            maxOvJobsFrom = 3000
            maxOvJobsTo = 1000
            # Adding atleast the  T1_ limits in the default config
            self.siteLimits = {'T1_CH_CERN':     {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 1000},
                               'T1_FR_CCIN2P3':  {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 1000},
                               'T1_RU_JINR':     {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 1000},
                               'T1_UK_RAL':      {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 1000},
                               'T1_US_FNAL':     {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 1000},
                               'T1_DE_KIT':      {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 1000},
                               'T1_IT_CNAF':     {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 1000},
                               'T1_ES_PIC':      {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 1000},
                               'T2_ES_IFCA':     {'maxOvJobsFrom': 3000,
                                                  'maxOvJobsTo'  : 0}}
        logging.debug("Config siteLimits: \n%s\n" % pformat(self.siteLimits))

        # The name and port of the HTCondor Collector to use:
        self.htCollector = self.__readCondorConfig__("JAT_OVERFLOW_collector", 'str')
        if not self.htCollector:
            # using the alias to the child collector here as a default value.
            # self.htCollector = "cmsgwms-collector-global.cern.ch:9620"
            # using the default collector configured for the current schedd as default value:
            self.htCollector = htcondor.param['COLLECTOR_HOST']
        logging.debug("HTCondor collector = %s" % self.htCollector)

        self.initMessage = "=================== An Overflow Configurator instance end! ==================="
        logging.debug(self.initMessage)

    def __readFileConfig__(self, fileName, fileParam, paramType='str'):
        """
        A function used to read a configuration parameter from a file
        """

        returnParam = False
        configDict = dict()
        if fileName and os.path.isfile(fileName):
            with open(fileName) as configFile:
                configDict = yaml.load(configFile)
                # configDict = json.load(configFile)
                configDict = ast.literal_eval(json.dumps(configDict)) # clean unicode chars from dict

        if fileParam not in configDict.keys():
            logging.warning("Config key: %s not set in file try different method." % fileParam)
        else:
            if paramType == 'dict':
                try:
                    returnParam = configDict[fileParam]
                except Exception as e:
                    returnParam = False
                    logging.exception("Could not convert %s to %s. Exception: %s" % (fileParam, paramType, e))

        return returnParam


    def __readCondorConfig__(self, condorParam, paramType='str'):
        """
        A function used to read a configuration parameter from condor_config
        """

        returnParam = False
        if condorParam not in htcondor.param.keys():
            logging.warning("Config key: %s not set in condor using the default value." % condorParam)
        else:
            if paramType == 'bool':
                try:
                    returnParam = strtobool(htcondor.param.get(condorParam))
                except Exception as e:
                    returnParam = False
                    logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))

            elif paramType == 'int':
                try:
                    returnParam = int(htcondor.param.get(condorParam))
                except Exception as e:
                    returnParam = False
                    logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))

            elif paramType == 'list':
                try:
                    returnParam = str(htcondor.param.get(condorParam))
                    returnParam = returnParam.upper()
                    returnParam = re.sub(',', ' ', returnParam).strip()
                    returnParam = returnParam.split()
                except Exception as e:
                    returnParam = False
                    logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))

            elif paramType == 'dict':
                try:
                    # todo - to figure out how to propagate a dict-like type from the condor config parameters
                    pass
                except Exception as e:
                    returnParam = False
                    logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))

            elif paramType == 'str':
                try:
                    returnParam = str(htcondor.param.get(condorParam))
                except Exception as e:
                    returnParam = False
                    logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))

            elif paramType == 'OverflowLevel' or paramType == 'OverflowType' :
                try:
                    # Here parsing the config string that comes from condorConfig
                    # and prefixing it with the className(in this case paramType)
                    # after that evaluating the achieved expression to gain
                    # the actual value of the bit flag.
                    # todo - this method should be moved to the class itself
                    prefixedStr = str(htcondor.param.get(condorParam)).upper()
                    prefixedStr = re.sub(r'[aA-zZ]+', (lambda x: "%s.%s" % (paramType, x.group())), prefixedStr)
                    returnParam = eval(prefixedStr)
                    logging.debug("prefixedStr = %s" % prefixedStr)
                    logging.debug("returned bit Flag = %s" % returnParam)
                except Exception as e:
                    returnParam = False
                    logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))

        return returnParam

class Overflow:
    """
    The major class for the Overflow.
    """

    def __init__(self):
        self.initMessage = "=================== An Overflow object instance start! ==================="
        logging.info(self.initMessage)

        # making all the needed instances for this class to work
        self.config = Configurator()

        self.schedd = htcondor.Schedd()
        self.htCollector =  htcondor.Collector(self.config.htCollector)

        self.infoCollector = InfoCollector(self.htCollector, self.config)
        self.estimator = Estimator(self.infoCollector, self.config, self.config.ovType)
        self.destGenerator = DestGenerator(self.infoCollector, self.config, self.config.ovType)


        self.requirementsString = None
        self.requirementsStringSet = set()
        self.currJobObj = None
        self.currSiteListDiff = None
        self.newSiteList = None
        self.currSiteList = None

        self.initMessage = "=================== An Overflow object instance end! ==================="
        logging.debug(self.initMessage)

    def prepareJobRouterAd(self, requirementsString, newSiteList, routeIdentifier):
        """
        A function used to prepare the routes to be added to the classad for the Jobrouter.
        This must be the only one to print something in the script's output,
        because that is how the Jobrouter is reading the new classads.
        everything else from the script must go in the relevant logging levels inside the logfile
        """

        if type(requirementsString) != str:
            logging.warning("Wrong type %s requirementsString of  %s" % (requirementsString.__class__, requirementsString))
            return None

        routerEntry = classad.ClassAd()
        # done - to check if all the  classaads generated for the JobRouter need to have unique names:
        #        must be unique so overflowLevel is not a good  identifier

        classadName = "Overflow: %s" % routeIdentifier
        routerEntry["Name"] = classadName
        routerEntry["OverrideRoutingEntry"] = True
        routerEntry["EditJobInPlace"] = True
        routerEntry["TargetUniverse"] = 5
        routerEntry["Requirements"] = classad.ExprTree(str(requirementsString))
        routerEntry["set_DESIRED_SITES"] = newSiteList
        routerEntry["set_DESIRED_SITES_Orig"] = self.currSiteList
        routerEntry["set_DESIRED_SITES_Diff"] = self.currSiteListDiff
        routerEntry["set_HasBeenOverflowRouted"] = True;
        routerEntry["set_RouteType"] = "overflow";
        routerEntry["eval_set_LastRouted"] = classad.ExprTree('time()')

        # if htcondor.param["DAGMAN_MAX_JOBS_SUBMITTED"] == 0:
        #     routerEntry["MaxJobs"] = self.maxJobsDefault
        # else:
        #     routerEntry["MaxJobs"] =  htcondor.param["DAGMAN_MAX_JOBS_SUBMITTED"]
        # if htcondor.param["DAGMAN_MAX_JOBS_IDLE"] == 0:
        #     routerEntry["MaxIdleJobs"] = self.maxJobsDefault
        # else:
        #     routerEntry["MaxIdleJobs"] = htcondor.param["DAGMAN_MAX_JOBS_IDLE"]

        logging.info("A new Route has been added:  %s" % routerEntry)
        print(routerEntry)

    # def routeUnifier(self, requirementsString):
    def isRouteUnique(self, requirementsString, mode='check'):
        """
        if mode == 'check' just check if the route exists in the set of already generated routes
        if mode == 'add' check and add the route to the set of route identifiers
        """
        unique = False
        requirementsString = requirementsString.lower()
        if mode.lower() == 'check':
            if requirementsString not in self.requirementsStringSet:
                unique = True
        elif mode.lower() == 'add':
            if requirementsString not in self.requirementsStringSet:
                self.requirementsStringSet.add(requirementsString)
                unique = True
        else:
            logging.debug("isRouteUnique: not recognized run mode: '%s' assuming 'CHECK'" % mode)
            if requirementsString not in self.requirementsStringSet:
                unique = True
        return unique

    def overflow(self, jobsInThisSchedd, ovLevel=OverflowLevel.PERTASK, ovType=OverflowType.GEO):

        # Objects initialisations moved to __init__()
        # infoCollector=InfoCollector()
        # estimator=Estimator(infoCollector)
        # destGenerator=DestGenerator(infoCollector)

        for clusterId, jobObj in jobsInThisSchedd.iteritems():
            self.currJobObj = jobObj
            # Set the self.currJobObj  var to the current job in order to extract classads from other functions
            # Set the self.currSiteListDiff var to be used later as the DESIRED_SITES_Diff classad (
            # This one cannot be added as a field to the self.currJobObj - it is a condor obj
            requirementsString = """((CMS_ALLOW_OVERFLOW isnt undefined) && (CMS_ALLOW_OVERFLOW == "True") && (HasBeenOverflowRouted is undefined))"""
            self.currSiteListDiff = None
            self.newSiteList = None
            self.currSiteList = None

            if ovLevel & OverflowLevel.PERTASK:
                requirementsString = """((target.Crab_ReqName == "%s") && %s)""" % (jobObj["CRAB_ReqName"], requirementsString)
                logging.debug("OverflowLevel.PERTASK: requirementsString: %s" % requirementsString)

            if ovLevel & OverflowLevel.PERJOB:
                requirementsString = """((target.ClusterId == "%s") && %s)""" % (jobObj["ClusterId"], requirementsString)
                logging.debug("OverflowLevel.PERJOB: requirementsString: %s" % requirementsString)

            if ovLevel & OverflowLevel.PERBLOCK:
                pass

            if ovLevel & OverflowLevel.PERFILE:
                pass

            if ovLevel & OverflowLevel.PERDATASET:
                pass

            if self.estimator.needOverflow(jobObj):
                # requirementsString = """((target.Crab_ReqName == "%s") && ((CMS_ALLOW_OVERFLOW isnt undefined) && (CMS_ALLOW_OVERFLOW == "True") && (HasBeenOverflowRouted is undefined)))""" % jobObj["CRAB_ReqName"]
                # requirementsString = re.sub('\s+', ' ', requirementsString).strip()

                # done - to move the actual route generation outside this 'if'
                #        only the requirementsString should obey the
                #        OverflowLevel flags. The Estimator and Destgenerator
                #        should be tied to the OverflowType flags
                if self.isRouteUnique(requirementsString, 'check'):
                    self.currSiteList = jobObj["DESIRED_SITES"]
                    self.newSiteList = self.destGenerator.destSiteList(self.currSiteList)
                    logging.debug("newSiteList: %s" % self.newSiteList)
                    # once we check that the 'current' and the 'new' SiteLists differ
                    # only then we create the route and add its identifier to the
                    # set that keeps track of the already generated routes. In the previous
                    # step we only check if the route is unique but we do not update
                    # the set of unique route identifiers
                    self.currSiteListDiff = self.destGenerator.siteListDiff(self.currSiteList, self.newSiteList)
                    if bool(self.newSiteList) and bool(self.currSiteListDiff):
                        self.isRouteUnique(requirementsString, 'add') and self.prepareJobRouterAd(requirementsString, self.newSiteList, jobObj["CRAB_ReqName"])

        logging.debug("self.requirementsStringSet: \n%s\n" % pformat(self.requirementsStringSet))
        # todo - to reestimate which are the loaded sites from the desiredsiteslist and refine the list && exclude
        #        the origin site (the origin may be more than one site - the whole set in desired sites list)

    def run(self):
        """
        The function used to run the class.
        """
        results = []
	try:
            # todo - to add the common constraint from the 'requirementsString'
            #        here too, because otherwise we are doing empty iterations
            #        through already routed tasks and always take decision not to
            #        route them again (which is expected if the rest of the
            #        script is working fine)
            # schedd object initialisation moved to __init__()
            # schedd = htcondor.Schedd()
            constraint = """
            (JobUniverse == 5) &&
            (CRAB_UserHN != "sciaba") &&
            (CRAB_SplitAlgo =!= "Automatic")
            """
            projection = ["ClusterId",
                          "CRAB_UserHN",
                          "CRAB_ReqName",
                          "DAGManJobId",
                          "JobStatus",
                          "JobUniverse",
                          "ServerTime",
                          "QDate",
                          "CMS_ALLOW_OVERFLOW",
                          "DESIRED_Overflow_Region",
                          "DESIRED_SITES"]

            results = self.schedd.query( constraint, projection)

        except Exception as e:
	    warnings.warn( e ,category=ScheddAlarm)

        # Creating a dict of classad.objects from the query result with keys = CRAB_RegName (to be used in the search later).
        jobsInThisSchedd = {}
        for jobAd in results:
            jobsInThisSchedd[jobAd["ClusterId"]] = jobAd

        # logging.debug("jobsInThisSchedd: \n%s\n" pformat( jobsInThisSchedd))

        # self.overflow(jobsInThisSchedd, OverflowLevel.PERJOB | OverflowLevel.PERTASK)
        self.overflow(jobsInThisSchedd, self.config.ovLevel, self.config.ovType)

        ################################################################
        # testing the cacheLifeTime expiration:
        # logging.debug("-------- 1. calling Overflow --------")
        # self.overflow(jobsInThisSchedd, OverflowLevel.PERJOB | OverflowLevel.PERTASK)
        # self.requirementsStringSet = set()
        # sleep(15)
        # logging.debug("-------- 2. calling Overflow --------")
        # self.overflow(jobsInThisSchedd, OverflowLevel.PERJOB | OverflowLevel.PERTASK)
        ################################################################


def main():
    overflow = Overflow()
    overflow.run()

if __name__ == "__main__":
    warnings.resetwarnings()
    warnings.simplefilter("always", category=GwmsmonAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=ScheddAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=SiteLimitsAlarm, lineno=0, append=False)

    fromatString = '%(asctime)s:%(levelname)s:%(module)s,%(lineno)d:%(message)s'
    logFormatter = logging.Formatter(fromatString)
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    # Setting different loglevels for file logging and console output
    fileHandler = logging.FileHandler(LOG_FILE)
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel(logging.DEBUG)
    rootLogger.addHandler(fileHandler)

    # Setting the output of the StreamHandler to stdout
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    consoleHandler.setLevel(logging.CRITICAL)
    rootLogger.addHandler(consoleHandler)

    # Temporary redirect stderr
    # sys.stderr = open('/dev/null', 'w')

    logging.basicConfig(filename=LOG_FILE,level=logging.INFO,format=fromatString)
    main()

