import logging
import warnings
from datetime import datetime 

class Alarm(Warning):
    """Base class for all the alarms raised by this script(module)."""
    # todo - in the global space to create a common buffer (a list) for holding all the alarms 
    #        and here to create a method for filling these alarms in the buffer. 
    #        At the end of the script this buffer must be sent by email 
    def __init__(self):
        self.message = "Base Alarm Class"
        logging.warning(message)
        # print("WARNING: %s" % message)

class StaleTaskAlarm(Alarm):
    """
    An Alarm raised for errors concerning Stale Tasks - all dags that live in the 10d 'LeaveJobInQueue' period 
    (CRAB_TaskSubmitTime - current ServerTime > 30days) - they should be in one of the following statuses: 
    Hold(5)-H|Comleted(4)-C|Removed(3)-X. This alarm is raised if a dag from that period is not in one of these statuses.
    (JobUniverse == 7)
    """
    def __init__(self, dag):
        self.dag = dag
        self.alarmMessage = ("StaleTaskAlarm: A Task in non final state found. Task clusterId: %s Task name: %s. Task['JobStatus']: %s" % (self.dag.clusterId, self.dag.name, self.dag.task["JobStatus"]))
        logging.info(self.alarmMessage)
        # print("StaleTaskAlarm: %s" % self.alarmMessage)


class ScheddAlarm(Alarm):
    """
    An Alarm raised if we couldn't make `condor_q` for any reason.
    (this alarm is an 'error' warning which is a real exception this could be  changed by
    setting this to one of'(always|default|once|ignore|module)' in warnings.filterwarnings())

    Attributes:
    time -- a time string
    """
    # todo - here to send emails to the operator or lemon alarms - for the moment the mails are going to be sent by the cronjobwrapper - which is capturing the WARN | ERR messages from the script output
    def __init__(self, message):
        self.message = message
        self.time = str(datetime.now())
        self.alarmMessage = ("ScheddAlarm: Could not query the schedd at: %s / additional info: %s" % (self.time,self.message))
        logging.error(self.alarmMessage)
        # print("ScheddAlarm: %s" % self.alarmMessage)

class DeletionAlarm(Alarm):
    """
    An Alarm raised if we couldn't make `condor_q` for any reason.
    (this alarm is an 'error' warning which is a real exception this could be  changed by
    setting this to one of'(always|default|once|ignore|module)' in warnings.filterwarnings())

    Attributes:
    time -- a time string
    """
    def __init__(self, dirToDel):
        self.dirToDel = dirToDel
        self.alarmMessage = ("DeletionAlarm: Could not delete the dir: %s " % self.dirToDel)
        logging.warning(self.alarmMessage)
        # print("DeletionAlarm: %s" % self.alarmMessage)


class KeyAlarm(Alarm):
    """
    An Alarm raised for errors concerning missing keys in the task classads found in the current schedd.
    (JobUniverse == 7)
    
    Attributes:
    message -- explanation of the error
    """
    def __init__(self, key, clusterId):
        self.key = key
        self.clusterId = clusterId
        self.alarmMessage = ("KeyAlarm: Missing key ['%s'] for the task with clusterId: %s" % (self.key, self.clusterId))
        logging.warning(self.alarmMessage)
        # print("KeyAlarm: %s" % self.alarmMessage)

