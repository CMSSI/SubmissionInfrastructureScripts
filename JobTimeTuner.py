#!/usr/bin/python

"""
This script/module is doing time tuning of the tasks in the CRAB3 Schedds.
All the information about job/task statistics is collected in ElasticSearch.
For gaining that information cms-gwmsmon.cern.ch is providing an API to make queries to ES.
cms-gwmsmon.cern.ch is giving the chance to make queries per user, per task or for all the tasks known to ES 
(the last type is cached in cms-gwmsmon.cern.ch and is refreshed every 3 min.)

1. The script is querying cms-gwmsmon.cern.ch for the information for all the tasks.
The expected format of the output from the query to cms-gwmsmon.cern.ch is:

 "170427_075802:sciaba_crab_hc-115-anysite-46278-20170426095902-t2_br_sprace" : Lower-cased CRAB_ReqName
 "count"    : count of entries in ES(ElasticSearch) - currently only nonidle jobs are reported to ES
 "QueryTime": key, which gives a timestamp when query was performed.
 "values"   : dictionary of percentiles calculated for the period of 720 min per task
 {
     "170427_075802:sciaba_crab_hc-115-anysite-46278-20170426095902-t2_br_sprace": {
         "count": 30,
         "values": {
             "50.0": 0.26111111111111113,
             "75.0": 0.32166666666666671,
             "95.0": 0.36143055555555553,
             "99.0": 0.36845
         }
     },
     .
     .
     .
     "170427_080957:mrivero_crab_test": {
         "count": 10,
         "values": {
             "50.0": 0.030555555555555555,
             "75.0": 0.03097222222222222,
             "95.0": 0.034000000000000002,
             "99.0": 0.034800000000000005
         }
     },
     "QueryTime": 1493282208
 }

Note:
The task names are converted to lowercase and are of type 'unicode',
but in the ["Name"] entry in the classad.objects are of type 'string' - a conversion is needed

2. After that the script is searching from all the tasks running in the current schedd which one 
is eligible for TimeTunning.
3. The new value for the task walltime is calculated and is put in the job classadd EstimatedWallTimeMins.
It is done by the Condor JobRouter. The jobs are modified in place (not creating a copy of the job). 
The values for EstimatedWallTimeMins are rounded in steps of 5 min.and are refreshed every 3 hours.
(a few parameters are provided for changing that behaviour). It must be noted that the script is querying
the the schedd for the tasks(jobuniverse == 7) but is modifying the jobs (jobuniverse == 5).

"""
from __future__ import print_function, division

import htcondor
import classad

import re
import sys
import os
import pwd
import json
import httplib, urllib2
import pprint
import logging
import warnings
import math
import JobAutoTunerAlarms

from JobAutoTunerAlarms import *

import pdb

import time
from datetime import datetime
from distutils.util import strtobool

# Setting log file location and formatting details
LOG_FILE = '/var/log/crab/JobTimeTuner.log'

def getUserName():
        return pwd.getpwuid( os.getuid())[0]
USERNAME = getUserName()


# todo - not to be repeated from JobAutoTuner but to make it a common function
def readCondorConfig(condorParam, paramType):
        returnParam = False
        if condorParam not in htcondor.param.keys():
                logging.warning("Config key: %s not set in condor using the default value." % condorParam)
        else:
                if paramType == 'bool':
                        try:
                                returnParam = strtobool(htcondor.param.get(condorParam))
                        except Exception as e:
                                returnParam = False
                                logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))
                elif paramType == 'int':
                        try:
                                returnParam = int(htcondor.param.get(condorParam))
                        except Exception as e:
                                returnParam = False
                                logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))
                elif paramType == 'float':
                        try:
                                returnParam = float(htcondor.param.get(condorParam))
                        except Exception as e:
                                returnParam = False
                                logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))
                elif paramType == 'str':
                        try:
                                returnParam = str(htcondor.param.get(condorParam))
                        except Exception as e:
                                returnParam = False
                                logging.exception("Could not convert %s to %s. Exception: %s" % (condorParam, paramType, e))
        return returnParam


class JobTimeTuner:

    """ A Class used for calculating/tuning the EstimatedWallTimeMins classad through the condor JobRouter. """

    def __init__(self):

        logging.info("JoubTimeTuner.py has been started at: %s, by username: %s" % (str(datetime.now()),USERNAME))

        # The minimum number of finished jobs before we consider the task as eligible for time tuning
        # self.minTaskStat = 20
        self.minTaskStat = readCondorConfig("JAT_TIMETUNER_minTaskStat", 'int')
        if not self.minTaskStat:
                self.minTaskStat = 20
        logging.debug("minTaskStat = %s"  % self.minTaskStat)

        # The minimum value allowed for the EstimatedWallTimeMins (min.)
        self.minWallTime = readCondorConfig("JAT_TIMETUNER_minWallTime", 'int')
        if not self.minWallTime:
                self.minWallTime = 10
        logging.debug("minWallTime = %s" %  self.minWallTime)

        # The maximum value allowed for the EstimatedWallTimeMins (min.)
        self.maxWallTime = readCondorConfig("JAT_TIMETUNER_maxWallTime", 'int')
        if not self.maxWallTime:
                self.maxWallTime = 48*60
        logging.debug("maxWallTime = %s" % self.maxWallTime)

        # The max step in min for the walltime rounding (min.)
        # wallTimeStep = self.minWallTime
        self.wallTimeStep = readCondorConfig("JAT_TIMETUNER_wallTimeStep", 'int')
        if not self.wallTimeStep:
                self.wallTimeStep = 60
        logging.debug("wallTimeStep = %s" % self.wallTimeStep)

        # The default number of max jobs that are going to be matched by the Jobrouter Classad
        # in principle we try to read these from the schedd config - DAGMAN_MAX_JOBS_SUBMITTED && DAGMAN_MAX_JOBS_IDLE
        # we refer to this var only if it is not set or == 0 there
        self.maxJobsDefault = readCondorConfig("JAT_TIMETUNER_maxJobsDefault", 'int')
        if not self.maxJobsDefault:
                self.maxJobsDefault = 10000
        logging.debug("maxJobsDefault = %s" % self.maxJobsDefault)

        # The refresh interval for the EstimatedWallTimeMins recalculation (this interval is in sec.) 
        # only jobs that have been idle for that amount of time after their previous time tuning will match
        # we have to lower this from 3*60*60 to no more than 20*60, because we  are now 
        # generating  the rules even for tasks with low statistics and recalculating 
        # the time after we gain more statistics and if we keep this big we will miss
        # all the jobs
        self.refreshInterval = readCondorConfig("JAT_TIMETUNER_refreshInterval", 'int')
        if not self.refreshInterval:
                self.refreshInterval = 20*60
        logging.debug("refreshInterval = %s" % self.refreshInterval)

	# The maximum age of the data provided by gwmsmon in seconds.
	# If we get data that is older than this period of time we raise a GwmsmonAlarm (sec.)
        self.gwmsmonMaxInfoAge = readCondorConfig("JAT_TIMETUNER_gwmsmonMaxInfoAge", 'int')
        if not self.gwmsmonMaxInfoAge:
                self.gwmsmonMaxInfoAge = 1*60*60
        self.gwmsmonInfoAge = self.gwmsmonMaxInfoAge + 1
        logging.debug("gwmsmonMaxInfoAge = %s" % self.gwmsmonMaxInfoAge)

        # A map between function aliases and internal function representations
        # It is used to point to the correct function to be used by corrFactor
        # function call. Any new corrMethod that is going to be created must be listed
        # here too, otherwise it will not be im posibble to reference it.
        self.fmap={
            'log': self.logCorr,
            'static': self.statCorr
        }

        # done - to make the function reference here and pass directly self.corrMethod to corrFactor
        # The default method to be used from crrFactor:
        self.corrMethodAlias = readCondorConfig("JAT_TIMETUNER_corrMethod", 'str')
        if not self.corrMethodAlias:
                self.corrMethodAlias = 'static'
        self.corrMethod=self.fmap[self.corrMethodAlias]
        logging.debug("corrMethodAlias = %s" % self.corrMethodAlias)
        logging.debug("corrMethod = %s" % self.corrMethod)

        # done - to make it puppet configurable
        # The bounderies of the corrFactor:
        self.minCorrFactor = readCondorConfig("JAT_TIMETUNER_minCorrFactor", 'float')
        self.maxCorrFactor = readCondorConfig("JAT_TIMETUNER_maxCorrFactor", 'float')
        if not self.minCorrFactor:
                self.minCorrFactor = 1.0
        if not self.maxCorrFactor:
                self.maxCorrFactor = 5.0
        logging.debug("minCorrFactor = %s" % self.minCorrFactor)
        logging.debug("maxCorrFactor = %s" % self.maxCorrFactor)

        # Default runMode: 'tag' || 'real'
        # in tagMode  we edit only EstimatedMaxWallTimeMins
        # in realMode we edit also MaxWallTimeMins
        self.runMode = readCondorConfig("JAT_TIMETUNER_runMode", 'str')
        if not self.runMode:
                self.runMode = 'tag'
        self.realMode = False
        if self.runMode == 'real': self.realMode = True
        logging.debug("runMode = %s" % self.runMode)

    def getTaskStat(self, task, user, qtype):

        """Get Task Stat from gwmsmon"""

        url = "cms-gwmsmon.cern.ch"
        if   qtype == 'peruser':
            urn = "/analysisview/json/%s/summary" % (user)
        elif qtype == 'pertask':
            urn = "/analysisview/json/historynew/percentileruntime720/%s/%s" % (user, task)
        elif qtype == "all":
            urn = "/analysisview/json/tasktime/percentiles"
        taskUrl = "http://%s%s" % (url, urn)
        opener = None
        ff = None
        try:
            req = urllib2.Request(taskUrl)
            logging.debug("The URL that we are about to open is: %s" % taskUrl)
            opener = urllib2.build_opener()
            f = opener.open(req)
            ff = json.loads(f.read())
        except urllib2.URLError as e:
            warnings.warn("urllib2.URLError exception while quering gwmsmon: %s" % e, category=GwmsmonAlarm)
        except Exception as e:
            warnings.warn("Unhandled exception while quering gwmsmon: %s" % e, category=GwmsmonAlarm)
        finally:
            if opener: opener.close()
        logging.debug("gwmsmon result: \n%s" % pprint.pformat(ff))
        return ff

    def prepareJobRouterAd(self, task, newTime, jobCount=0):
        """
        A function used to prepare the routes to be added to the classad for the Jobrouter. 
        This must be the only one to print something in the script's output,
        because that is how the Jobrouter is reading the new classads.
        everything else from the script must go in the relevant logging levels inside the logfile
        """
        routerEntry = classad.ClassAd()
        # print(task.__class__) # produces an error if task.__class__ == 'unicode'
        if type(task) != str:
            logging.warning("Wrong type %s of taskName %s" % (task.__class__, task))
            return None
        # done - to try once again to add the HasBeenTimingTuned constrain to the Requirenments sctring:
        # requirementsString = '((target.CRAB_ReqName == "%s") && (target.JobUniverse == 5) && (HasBeenTimingTuned =!= true))' % task
        # done - moving to better fine tuning during the jobruntime:
        requirementsString = '((target.CRAB_ReqName == "%s") && (target.JobUniverse == 5) && ((LastTimingTuned is undefined) || (LastTimingTuned < time() - %d)))' % (task,self.refreshInterval)
        classadName = "JobTimeTuner: %s" % task
        routerEntry["Name"] = classadName
        routerEntry["OverrideRoutingEntry"] = True
        routerEntry["EditJobInPlace"] = True
        routerEntry["TargetUniverse"] = 5
        routerEntry["Requirements"] = classad.ExprTree(str(requirementsString))
        routerEntry["set_EstimatedWallTimeMins"] = newTime
        if self.realMode: routerEntry["set_MaxWallTimeMins"] = newTime
        routerEntry["set_EstimatedWallTimeJobCount"] = jobCount
        routerEntry["set_HasBeenRouted"] = False;
        routerEntry["set_HasBeenTimingTuned"] = True;
        routerEntry["eval_set_LastTimingTuned"] = classad.ExprTree('time()')

        if htcondor.param["DAGMAN_MAX_JOBS_SUBMITTED"] == 0:
            routerEntry["MaxJobs"] = self.maxJobsDefault
        else:
            routerEntry["MaxJobs"] =  htcondor.param["DAGMAN_MAX_JOBS_SUBMITTED"]
        if htcondor.param["DAGMAN_MAX_JOBS_IDLE"] == 0:
            routerEntry["MaxIdleJobs"] = self.maxJobsDefault
        else:
            routerEntry["MaxIdleJobs"] = htcondor.param["DAGMAN_MAX_JOBS_IDLE"]

        logging.info("A new Route has been added:  %s" % routerEntry)
        print(routerEntry)

    def logCorr(self, jobCount, minTaskStat, *args, **kwargs):
        """
        The function used for applying a correction factor to the estimated time
        It returns a logarithmic correction factor in order to compensate the
        badly estimated time when we tune tasks with low statistics (which are
        within the region between 1 and self.minTaskStat finished jobs. It has
        a reverse dependency on self.minTaskStat and a forward dependency on
        jobCount. Shrotly speaking it is:

        log(self.minTaskStat, base=jobCount)

        the function is normalized (cut between) min = 1 && max = 5

        """
        # todo - to start using logTaskStat instead of minTaskStat
        corrFactor = self.minCorrFactor

        if jobCount > self.minTaskStat:
                logging.debug("logCorr: jobCount = %s, high statistics corrFactor = %s" % (jobCount, corrFactor))
                return corrFactor
        if jobCount == 0 or jobCount == 1:
                corrFactor = self.maxCorrFactor
                logging.debug("logCorr: jobCount = %s, normalizing to max corrFactor = %s" %  (jobCount, corrFactor))
                return corrFactor

        corrFactor = math.log(minTaskStat, jobCount)
        corrFactor = max(corrFactor, self.minCorrFactor)
        corrFactor = min(corrFactor, self.maxCorrFactor)
        logging.debug("logCorr: jobCount = %s, low statistics corrFactor = %s" %  (jobCount, corrFactor))
        return corrFactor

    def statCorr(self, jobCount, minTaskStat, *args, **kwargs):
        corrFactor=self.minCorrFactor
        logging.debug("statCorr: jobCount = %s, static corrFactor = %s" %  (jobCount, corrFactor))
        return corrFactor

    def corrFactor(self, *args, **kwargs):
        # setting the defaultfunction call if it is missing
        corrMethod=kwargs.pop('corrMethod', self.corrMethod)
        return corrMethod(*args, **kwargs)

        #funcall=self.fmap[corrMethod]
        #return funcall(*args, **kwargs)

    def jobTimeTuning(self, tasksInThisSchedd, taskStatAll):
        """
        A function that is used to find the tasks that need to be modified from
        the list of tasks that are running on the schedd. It searches every task
        from the schedd in the tasks that are known to gwmsmon. It expects two
        dicsts as input one from gwmsmon one from htcondor and is making key
        comparision to avoid type conversion from unicode to string, that is why
        the keyname must be the same as the taskname.
        """
        # todo - to make these type checkings exceptions
        if type(tasksInThisSchedd) != dict:
            logging.warning("Wrong type %s of tasksInThisSchedd" % tasksInThisSchedd.__class__)
            return None
        if type(taskStatAll) != dict:
            logging.warning("Wrong type %s of taskStatAll" % taskStatAll.__class__)
            return None
        for scheddTask, scheddTaskVars in tasksInThisSchedd.iteritems():
            logging.debug("searching for name %s " % scheddTask.lower())
            for esTask, esTaskVars in taskStatAll.iteritems():
                if scheddTask.lower() == esTask.lower():
                    logging.debug("%s == %s" % (scheddTask.lower(),esTask.lower()))
                    # the following line is not needed anymore:
                    # it is compensated by the 'logarithmic correction factor'
                    # if esTaskVars['count'] < self.minTaskStat : continue
                    logging.debug("%s -> scheddTaskVars['MaxWallTimeMins:']: %s" % (scheddTaskVars["CRAB_ReqName"],scheddTaskVars["MaxWallTimeMins"]) )
                    logging.debug("%s -> esTaskVars['values']['95.0']: %s" % (esTask, type(esTaskVars['values']['95.0'])))
                    logging.debug("%s -> esTaskVars['values']['95.0']: %s" % (esTask, esTaskVars['values']['95.0']))
                    # done - here to add type conversion and checking for the value returned in esTaskVars['values']['95.0']
                    if type(esTaskVars['values']['95.0']) != float:
                        logging.warning("esTaskVars['values']['95.0']: %s -> Assuming esTaskVars['values']['95.0']: 0.0000000" % type(esTaskVars['values']['95.0']))
                        esTaskVars['values']['95.0'] = 0

                    esTime = esTaskVars['values']['95.0']

                    # todo - to make it puppet configurable:
                    # applying an upper limit of 24h esTime for acting on jobs
                    # if the esTime > 24h simply do nothing:
                    # https://cms-logbook.cern.ch/elog/Analysis+Operations/2076
                    if esTime > 24: continue

                    # # applying the logarithmic correction factor
                    # esTime = esTime*self.logCorr(esTaskVars['count'], self.minTaskStat)

                    # applying the correction factor
                    # And passing also the task ClassAdd in order to create more elaborate functions depending on more parameters
                    corrFactor = self.corrFactor(jobCount=esTaskVars['count'], minTaskStat=self.minTaskStat, corrMethod=self.corrMethod, taskAd=scheddTaskVars)
                    esTime = esTime*corrFactor

                    # rounding the estimated Time
                    esTime = int(round(esTime*60 + 0.5))
                    # shifting to the nearest TIMESTEP
                    newTime  =  esTime - esTime%self.wallTimeStep +self.wallTimeStep

                    # limiting the newTime between minWallTime and (maxWallTime|MaxWallTimeMinsRun)
                    # the last one is taken from the task's classad not from the config parameters
                    newTime = max(newTime, self.minWallTime)
                    newTime = min(newTime, self.maxWallTime)

                    try:
                        newTime = min(newTime, scheddTaskVars['MaxWallTimeMinsRun'])
                        logging.debug("%s -> scheddTaskVars['MaxWallTimeMinsRun:']: %s" % (scheddTaskVars["CRAB_ReqName"],scheddTaskVars["MaxWallTimeMinsRun"]) )
                    except Exception as e:
                        keyAlarm = KeyAlarm(key='MaxWallTimeMinsRun', clusterId=scheddTaskVars['ClusterId'])
                        warnings.warn(keyAlarm, stacklevel=5)

                    logging.debug("newTime: %s" % newTime)
                    self.prepareJobRouterAd(scheddTaskVars["CRAB_ReqName"], newTime, esTaskVars['count'])
                    continue

    # done - to add a main function in order to integrate the rest of the functioanlities in a single script
    def run(self):
        """
        The function used to run the class.
        """
        taskStatAll = None
        taskStatAll = self.getTaskStat( task="", user="", qtype='all')
	# logging.debug(json.dumps(taskStatAll, sort_keys=True, indent=4, separators=(',', ': '))) # too much (unneeded) information from this dump
        if not taskStatAll:
            warnings.warn("gwmsnom.cern.ch returned an empty list of jobs/tasks",category=GwmsmonAlarm)

        self.gwmsmonInfoAge = (int(time.time()) - int(taskStatAll["QueryTime"]))
	if self.gwmsmonInfoAge > self.gwmsmonMaxInfoAge:
           warnings.warn(("gwmsnom.cern.ch provided Stale data. Last update from: %s " % time.ctime(taskStatAll["QueryTime"])),category=GwmsmonAlarm)

	for key, value in taskStatAll.items():
           if re.search('(^[eE][rR][rR].*|^[wW][aA][rR][nN].*)', key):
               warnings.warn(("gwmsnom.cern.ch returned an (Error|Warning) in the json file: %s " % value),category=GwmsmonAlarm)

	# if "Error" in taskStatAll.keys():
	#    warnings.warn(("gwmsnom.cern.ch returned an Error in the json file: %s " % taskStatAll["Error"]),category=GwmsmonAlarm)

	results = []
        constraint = """
        (TaskType =?= "ROOT" && JobStatus =?= 2) &&
        (CRAB_UserHN =!= "sciaba") &&
        (CRAB_SplitAlgo =!= "Automatic") &&
        (DESIRED_Sites =!= "T3_US_FNALLPC")
        """

        projection = ["ClusterId",
                      "CRAB_UserHN",
                      "CRAB_ReqName",
                      "DAGManJobId",
                      "JobStatus",
                      "JobUniverse",
                      "MaxWallTimeMins",
                      "MaxWallTimeMinsRun"]
	try:
            schedd = htcondor.Schedd()
            results = schedd.query(constraint, projection)
        except Exception as e:
	    warnings.warn( e ,category=ScheddAlarm)

        # Creating a dict of classad.objects from the query result with keys = CRAB_RegName (to be used in the search later).
        tasksInThisSchedd = {}
        for taskAd in results:
            tasksInThisSchedd[taskAd["CRAB_ReqName"]] = taskAd

        logging.debug("tasksInThisSchedd: \n%s" % pprint.pformat(tasksInThisSchedd))
        self.jobTimeTuning(tasksInThisSchedd, taskStatAll)

def main():
    jobTimeTuner = JobTimeTuner()
    jobTimeTuner.run()

if __name__ == "__main__":
    warnings.resetwarnings()
    warnings.simplefilter("always", category=GwmsmonAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=ScheddAlarm, lineno=0, append=False)
    warnings.simplefilter("always", category=KeyAlarm, lineno=0, append=False)

    fromatString = '%(asctime)s:%(levelname)s:%(module)s,%(lineno)d:%(message)s'
    logFormatter = logging.Formatter(fromatString)
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    # Setting different loglevels for file logging and console output
    fileHandler = logging.FileHandler(LOG_FILE)
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel(logging.DEBUG)
    rootLogger.addHandler(fileHandler)

    # Setting the output of the StreamHandler to stdout
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    consoleHandler.setLevel(logging.CRITICAL)
    rootLogger.addHandler(consoleHandler)

    # Temporary redirect stderr
    # sys.stderr = open('/dev/null', 'w')


    logging.basicConfig(filename=LOG_FILE,level=logging.INFO,format='%(asctime)s:%(levelname)s:%(module)s,%(lineno)d:%(message)s')
    main()
