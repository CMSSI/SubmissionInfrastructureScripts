# Submission Infrastructure Scripts
This repository contains various scripts being used in the submission infrastructure. If you are adding a new script, please do not forget to create a section in the README and explain its usage. 

If you need write access to the repository, please contact the submission infrastructure team at:
cms-submission-infrastructure@cern.ch

# CMSLPCRoute.py
This script works together with the HTCondor job router to create routes for the jobs whitelisting T3_US_FNALLPC. It creates a separate log file (/var/log/condor/CMSLPCRoute.log) detailing its operations. The script behaves as follows:

(1) It fetches the list of LPC users from the CE at FNAL. "CMSLPC_USER_URL" needs to be set in the HTCondor configurations and should point to the location where DN file is hosted.

(2) It then uses the fetched list of user DN to generate a route for the jobs in the queue.

(3) It also checks if a user tries to run at the LPC but isn't in the LPC authorized user list. In this case, the job is set to HOLD and an appropriate job hold reason is set.

# total_free_memory.sh
This script is used by an HTCondor as a hook to provide the free memory on a CRAB3 schedd.
The hook is configure on each schedd with the following lines in 99_local_tweaks.config:

```
SCHEDD_CRON_CONFIG_VAL = $(RELEASE_DIR)/bin/condor_config_val
SCHEDD_CRON_JOBLIST = $(SCHEDD_CRON_JOBLIST) TOTAL_FREE_MEMORY_MB
SCHEDD_CRON_TOTAL_FREE_MEMORY_MB_EXECUTABLE = /data/srv/SubmissionInfrastructureScripts//total_free_memory.sh
SCHEDD_CRON_TOTAL_FREE_MEMORY_MB_PERIOD = 300s
```
# JobAutoTuner.py 
This Script is intended to load separately the different scripts related
to job modifications done through the  condor JobRouter in the CRAB3 SChedd.
it is done in the following order:
1. JobTimeTuner.py
2. CMSLPCRoute.py

# JobTimeTuner.py
This script is doing time tuning of the tasks in the CRAB3 Schedds. 
All the information about job/task statistics is collected in ElasticSearch.
cms-gwmsmon.cern.ch is providing an API to make queries to ES.

1. The script is querying cms-gwmsmon.cern.ch for the information for all the tasks.
2. The script is searching from all the tasks running in the current schedd which one is eligible for TimeTunning. 
3. The new value for the task walltime is calculated  and is put in the job classadd EstimatedWallTimeMins through the condor JobRouter.
